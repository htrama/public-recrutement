package ch.ge.dcs.recrutementapp;

/**
 * Constantes utilisées dans le projet
 */
public class Constants {

    /**
     * le nombre de ligne retourné par défaut
     */
    public static final int NBR_PER_PAGE = 20;
    
}
